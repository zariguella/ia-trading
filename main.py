import json
import sys
from multiprocessing import Queue

from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QWidget, QHBoxLayout

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from mpl_finance import candlestick_ohlc

from iqoptionapi.stable_api import IQ_Option

from modelos.OperacionesModel import OperacionesModel
from threads.IAThread import IAThread
from threads.OperadorThread import OperadorThread
from threads.SignalsThread import SignalsThread
from threads.TradingThread import CandleThread
from util.operador import Operador

Ui_Window, QtBaseClass = uic.loadUiType("templates/principal.ui")

class Window(QtWidgets.QMainWindow, Ui_Window):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_Window.__init__(self)
        self.setupUi(self)
        self.window().setWindowTitle('El comerciante astuto')
        self.email = "dbermudez455@gmail.com"
        self.password = "Muuysecreta@123"
        self.operador = Operador(self.email, self.password, self)
        self.maxdict = 1

        self.operacionesModel = OperacionesModel()
        self.operacionesView.setModel(self.operacionesModel)

        self.operaciones = []
        self.fig = plt.figure()
        self.canvas = FigureCanvas(self.fig)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.fig2 = plt.figure()
        self.canvas2 = FigureCanvas(self.fig2)
        self.toolbar2 = NavigationToolbar(self.canvas2, self)

        # set the layout
        layout = QHBoxLayout()

        figura1 = QWidget()
        layout1 = QVBoxLayout()
        layout1.addWidget(self.canvas)
        layout1.addWidget(self.toolbar)
        figura1.setLayout(layout1)
        # set the layout

        figura2 = QWidget()
        layout2 = QVBoxLayout()
        layout2.addWidget(self.canvas2)
        layout2.addWidget(self.toolbar2)
        figura2.setLayout(layout2)

        self.diagramasLayout.addWidget(figura1)
        self.diagramasLayout.addWidget(figura2)
        #self.setLayout(layout)



        self.q_machine = Queue()
        self.q_prediccion = Queue()
        self.q_grafica = Queue()
        self.q_signals = Queue()

        self.candleThread = CandleThread(self.operador, self.q_machine, self)
        self.candleThread.signal.connect(self.actualizarBalance)
        self.candleThread.start()
        self.iaThread = IAThread(self.q_machine, self.q_prediccion)
        self.iaThread.start()
        self.operadorThread = OperadorThread(self.q_prediccion, self.q_signals, self)
        self.operadorThread.signal.connect(self.pintar)
        self.operadorThread.start()
        self.signalsThread = SignalsThread(self.operador, self.q_signals, self)
        self.signalsThread.start()

        self.ohlc = []
        self.ohlc_real = []
        self.canvas.set_window_title('Prediccion')
        self.ax1 = self.fig.add_subplot(111)
        self.ax1.xaxis.set_major_locator(mticker.MaxNLocator(10))
        self.ax1.grid(True)


        self.canvas2.set_window_title('Real')
        self.ax2 = self.fig2.add_subplot(111)
        self.ax2.xaxis.set_major_locator(mticker.MaxNLocator(10))
        self.ax2.grid(True)
        predicciones = 0

        self.balance = 0

    def actualizarBalance(self):
        pass

    def agregarOperacion(self, operacion):
        self.acertividadSpin.setValue((self.aciertosSpin.value()*100/self.prediccionesSpin.value()))
        self.operaciones.append(operacion)
        self.operacionesModel.actualizar(self.operaciones[::-1])


    def pintar(self):
        vela = self.operadorThread.vela
        self.ax1.clear()
        self.ax2.clear()
        self.ohlc.append(vela.get('prediccion'))
        self.ohlc_real.append(vela.get('real'))
        self.ohlc = self.ohlc[-50:]
        self.ohlc_real = self.ohlc_real[-50:]
        candlestick_ohlc(self.ax1, self.ohlc, width=0.4, colorup='#77d879', colordown='#db3f3f')
        self.canvas.draw()
        candlestick_ohlc(self.ax2, self.ohlc_real, width=0.4, colorup='#77d879', colordown='#db3f3f')
        self.canvas2.draw()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())