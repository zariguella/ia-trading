import json
import time

from PyQt5.QtCore import QThread, pyqtSignal


class CandleThread(QThread):
    signal = pyqtSignal()

    def __init__(self, operador, q_machine, padre):
        QThread.__init__(self)
        self.q = q_machine
        self.operador = operador
        self.padre = padre

    def run(self):
        while 1:
            try:
                self.padre.balance = self.operador.get_balance()
                # size=[1,5,10,15,30,60,120,300,600,900,1800,3600,7200,14400,28800,43200,86400,604800,2592000,"all"]
                self.candle = list(self.operador.getCandle(
                    self.padre.instrumentoEdit.text(),
                    self.padre.periodoSpin.value(),
                    self.padre.maxdict
                ))
                self.candle = list(
                    map(
                        lambda x_i: [x_i.get('open'), x_i.get('close'), x_i.get('min'), x_i.get('max')], self.candle))
                self.candle = json.dumps(self.candle)
                self.q.put(self.candle)
                time.sleep(self.padre.periodoSpin.value())
                self.signal.emit()
            except Exception as e:
                print(e)

class OrderThread(QThread):
    signal = pyqtSignal()

    def __init__(self, operador, instrumento, cantidad, accion, duracion):
        QThread.__init__(self)
        self.operador = operador
        self.instrumento = instrumento
        self.cantidad = cantidad
        self.accion = accion
        self.duracion = duracion
        self.ident = None
        self.resp = None
        self.ckeck = False

    def run(self):
        try:
            self.ident = self.operador.buy_digital_spot(self.instrumento, self.cantidad, self.accion, self.duracion)
            while not self.check:
                self.check, self.resp = self.checkOrder(self.ident)
            self.signal.emit()
        except Exception as e:
            print(e)

    def checkOrder(self, ident):
        try:
            check, resp = self.operador.check_win_digital_v2(ident)
            # print(self.operador.get_digital_position(ident))
            print('resultado:' + self.operador.check_win_digital_v2(ident))
            return check, resp
        except Exception as e:
            print(e)
            time.sleep(1)
            return False, None

        def getStrike(self):
            self.operador.subscribe_strike_list(self.goal, self.duration)
            data = self.operador.get_realtime_strike_list(self.goal, self.duration)
            return data
