import json
import time
from multiprocessing import Process

from PyQt5.QtCore import QThread, pyqtSignal
from util.inteligencia_artificial import InteligenciaArtificial


class IAThread(QThread):
    signal = pyqtSignal()

    def __init__(self, q_machine, q_prediccion):
        QThread.__init__(self)
        self.ia = InteligenciaArtificial()
        self.q_machine = q_machine
        self.q_prediccion = q_prediccion

    def run(self):
        self.p = Process(target=self.p_ia, args=(self.q_machine, self.q_prediccion, ))
        self.p.start()

    def p_ia(self, q_machine, q_prediccion):
        self.ia.predictPrices(q_machine, q_prediccion)

