import json

from PyQt5.QtCore import QThread, pyqtSignal


class OperadorThread(QThread):
    signal = pyqtSignal()

    def __init__(self, q_prediccion, q_signals, padre):
        QThread.__init__(self)
        self.q_prediccion = q_prediccion
        self.q_signals = q_signals
        self.padre = padre
        self.vela = None

    def run(self):
        while True:
            element = self.q_prediccion.get()
            self.vela = json.loads(element)
            open = self.vela.get('real')[1]
            close = self.vela.get('real')[4]
            tendencia = close - open
            close_predict = self.vela.get('prediccion')[4]
            prediccion = close_predict - close
            if tendencia > 0 and prediccion > 0 and self.padre.alzaCheck.isChecked():
                signal = self.padre.instrumentoEdit.text(), self.padre.cantidadSpin.value(), 'call', self.padre.alzaSpin.value()
                signal = json.dumps(signal)
                self.q_signals.put(signal)
            if tendencia < 0 and prediccion < 0 and self.padre.bajaCheck.isChecked():
                signal = self.padre.instrumentoEdit.text(), self.padre.cantidadSpin.value(), 'put', self.padre.bajaSpin.value()
                signal = json.dumps(signal)
                self.q_signals.put(signal)
            self.signal.emit()

