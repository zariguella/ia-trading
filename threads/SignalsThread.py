import json

from PyQt5.QtCore import QThread, pyqtSignal


class SignalsThread(QThread):
    signal = pyqtSignal()

    def __init__(self, operador, q_signals, padre):
        QThread.__init__(self)
        self.operador = operador
        self.q_signals = q_signals
        self.padre = padre

    def run(self):
        while True:
            signal = self.q_signals.get()
            signal = json.loads(signal)
            orden = self.operador.makeOrder(signal[0], signal[1], signal[2], signal[3])
            while not self.q_signals.empty():
                self.q_signals.get()
