import time
import unittest
import os
from iqoptionapi.stable_api import IQ_Option
import logging

#logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')

class Operador:
    
    
    def __init__(self, email, password, padre):
        self.operador = IQ_Option(email, password)
        self.ronda = [1, 2, 5, 10]
        self.racha = 0
        self.padre = padre


    def makeOrder(self, instrumento, cantidad, accion, duracion):
        try:
            if self.padre.rondaCheck.isChecked():
                print(self.padre.rondaSpin.value())
                if self.racha >= self.padre.rondaSpin.value():
                    self.racha = 0
                cantidad = self.ronda[(self.racha%len(self.ronda))]
                self.padre.cantidadSpin.setValue(cantidad)
            ident = self.operador.buy_digital_spot(instrumento, cantidad, accion, duracion)
            self.padre.prediccionesSpin.setValue((self.padre.prediccionesSpin.value() + 1))
            operacion = {"ident":str(ident)+' - '+str(accion)+' - '+str(duracion)+'min', "win": 0}
            self.padre.agregarOperacion(operacion)
            check = False
            resp = None
            while not check:
                check, resp = self.checkOrder(ident)
                time.sleep(duracion)
            if resp is not None and float(resp) > 0:
                operacion['win'] = 1
                self.padre.aciertosSpin.setValue((self.padre.aciertosSpin.value() + 1))
                self.racha = 0
            else:
                self.racha += 1
                operacion['win'] = 2
            resultado = self.padre.resultadoSpin.value()+float(resp)
            self.padre.resultadoSpin.setValue(resultado)
            return ident
        except Exception as e:
            print(e)

    def checkOrder(self, ident):
        try:
            check, resp = self.operador.check_win_digital_v2(ident)
            #print(self.operador.get_digital_position(ident))
            print('resultado:'+str(self.operador.check_win_digital_v2(ident)))
            return check, resp
        except Exception as e:
            print(e)
            return False, None


    def getRealCandle(self, goal, size, maxdict):
        try:
            if self.operador.check_connect() == False:
                self.operador.connect()
            #size=[1,5,10,15,30,60,120,300,600,900,1800,3600,7200,14400,28800,43200,86400,604800,2592000,"all"]
            self.operador.start_candles_stream(goal, size, maxdict)
            cc = self.operador.get_realtime_candles(goal, size)
            self.operador.stop_candles_stream(goal, size)
            self.contador = 0
            return cc
        except Exception as e:
            print(e)


    def get_balance(self):
        return self.operador.get_balance()

    def getCandle(self, goal, size, maxdict):
        try:
            if self.operador.check_connect() == False:
                self.operador.connect()
            #size=[1,5,10,15,30,60,120,300,600,900,1800,3600,7200,14400,28800,43200,86400,604800,2592000,"all"]
            cc = self.operador.get_candles(goal, size, maxdict, time.time())
            return cc
        except Exception as e:
            print(e)
