# -*- coding: utf-8 -*-
from datetime import datetime
import json
import torch

class InteligenciaArtificial:

    def __init__(self,):
        self.PATH = 'entrenammiento_candles.model'
        self.D_in, self.N, self.H, self.D_out = 4, 4, 100, 4
        self.model = torch.nn.Sequential(
            torch.nn.Linear(self.D_in, self.N),
            torch.nn.Linear(self.N, self.H),
            torch.nn.ReLU(),
            torch.nn.Linear(self.H, self.D_out),
            torch.nn.Linear(self.D_out, 1),
        )
        try:
            self.model.load_state_dict(torch.load(self.PATH))
            self.model.eval()
        except Exception as e:
            print(e)
        self.loss_fn = torch.nn.MSELoss(reduction='sum')
        self.learning_rate = 1e-4
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)

    def predictPrices(self, q_machine, q_prediccion):
        tmp = 0
        x = []
        while 1:
            if len(x)> self.N:
                x = x[-self.N:]
                t = torch.tensor(x)
                y_pred = self.model(t)
                actual = q_machine.get()
                actual = json.loads(actual)
                print(actual)
                y_list = y_pred.tolist()
                fecha = datetime.now().timestamp()
                vela = {}
                vela['prediccion'] = fecha, y_list[0][0], y_list[3][0], y_list[2][0], y_list[1][0], 0
                vela['real'] = fecha, actual[0][0], actual[0][3], actual[0][2], actual[0][1], 0
                vela = json.dumps(vela)
                q_prediccion.put(vela)
                y = torch.tensor(actual[0])
                y = y.view(4, -1)
                loss = self.loss_fn(y_pred, y)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                torch.save(self.model.state_dict(), self.PATH)
            else:
                actual = q_machine.get()
                actual = json.loads(actual)
            x.append(actual[0])