import json

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from mpl_finance import candlestick_ohlc


def graph(q_grafica):
    ohlc = []
    ohlc_real = []
    fig = plt.figure(1)
    fig.canvas.set_window_title('Prediccion')
    ax1 = plt.subplot2grid((1, 1), (0, 0))
    ax1.xaxis.set_major_locator(mticker.MaxNLocator(10))
    ax1.grid(True)
    plt.xlabel('Date')
    plt.ylabel('Price')
    fig2 = plt.figure(2)
    fig2.canvas.set_window_title('Real')
    ax2 = plt.subplot2grid((1, 1), (0, 0))
    ax2.xaxis.set_major_locator(mticker.MaxNLocator(10))
    ax2.grid(True)
    plt.subplots_adjust(left=0.09, bottom=0.20, right=0.94, top=0.90, wspace=0.2, hspace=0)
    plt.ion()
    plt.show()
    predicciones = 0
    while True:
        element = q_grafica.get()
        vela = json.loads(element)
        ax1.clear()
        ax2.clear()
        ohlc.append(vela.get('prediccion'))
        ohlc_real.append(vela.get('real'))
        ohlc = ohlc[-50:]
        ohlc_real = ohlc_real[-50:]
        candlestick_ohlc(ax1, ohlc, width=0.4, colorup='#77d879', colordown='#db3f3f')
        fig.canvas.draw()
        candlestick_ohlc(ax2, ohlc_real, width=0.4, colorup='#77d879', colordown='#db3f3f')
        fig2.canvas.draw()
