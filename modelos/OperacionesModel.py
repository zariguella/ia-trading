from PyQt5 import QtCore
from PyQt5.QtGui import QIcon


class OperacionesModel(QtCore.QAbstractTableModel):
    def __init__(self, *args, **kwargs):
        super(OperacionesModel, self).__init__(*args, **kwargs)
        self.elementos = []
        self.header = ['Operacion']


    def rowCount(self, parent):
        if self.elementos is None:
            return 0
        return len(self.elementos)


    def columnCount(self, parent):
        return 1


    def data(self, index, role):
        if self.elementos is not None:
            elemento = self.elementos[index.row()]
            if role == QtCore.Qt.DisplayRole:
                if index.column() == 0:
                    return elemento.get('ident')
            if role == QtCore.Qt.DecorationRole:
                if elemento.get('win') == 0:
                    return QIcon('icons/task-ongoing.png')
                if elemento.get('win') == 1:
                    return QIcon('icons/task-complete.png')
                if elemento.get('win') == 2:
                    return QIcon('icons/task-reject.png')


    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.header[col]
        return None

    def actualizar(self, elementos):
        try:
            self.beginResetModel()
            self.elementos = elementos
            self.endResetModel()
        except Exception as e:
            print(e)