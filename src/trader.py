# -*- coding: utf-8 -*-
import json
import time

import numpy
from oandapyV20 import API
from oandapyV20.exceptions import V20Error, StreamTerminated
from oandapyV20.endpoints.pricing import PricingStream
from util.exampleauth import exampleAuth
from requests.exceptions import ConnectionError
from multiprocessing import Process, Queue
import torch

accountID, access_token = exampleAuth()
MAXREC = 0
TICK = 15
api = API(access_token=access_token, environment="practice")
r = PricingStream(accountID=accountID, params={"instruments": ",".join(['BTC_USD'])})
#r = PricingStream(accountID=accountID, params={"instruments": ",".join(['AUD_JPY'])})

PATH = 'entrenammiento_n.model'
# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
D_in, N, H, D_out = 2, 100, 100, 2
model = torch.nn.Sequential(
    torch.nn.Linear(D_in, N),
    torch.nn.Linear(N, H),
    torch.nn.ReLU(),
    torch.nn.Linear(H, D_out),
    torch.nn.Linear(D_out, 1),
)
try:
    model.load_state_dict(torch.load(PATH))
    model.eval()
except Exception as e:
    print(e)
loss_fn = torch.nn.MSELoss(reduction='sum')
learning_rate = 1e-4
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)


class Trader:

    def __init__(self):
        self.tack = 60

    def main(self):
        q = Queue()
        # p = Process(target=self.getPrices, args=(q,))
        # p.start()
        p_tick = Process(target=self.tick, args=(q,))
        p_tick.start()
        r = Process(target=self.predictPrices, args=(q,))
        r.start()

    def tick(self, q):
        i = 0
        while True:
            time.sleep(self.tack)
            q.put({"tick":i})
            i += 1


    def getPrice(self):
        try:
            for R in api.request(r):
                if R.get('type') == 'PRICE':
                    return R

        except V20Error as e:
            # catch API related errors that may occur
            with open("LOG", "a") as LOG:
                LOG.write("V20Error: {}\n".format(e))
        except ConnectionError as e:
            with open("LOG", "a") as LOG:
                LOG.write("Error: {}\n".format(e))
        except StreamTerminated as e:
            with open("LOG", "a") as LOG:
                LOG.write("Stopping: {}\n".format(e))
        except Exception as e:
            with open("LOG", "a") as LOG:
                LOG.write("??? : {}\n".format(e))

    def getVela(self, t):
        p = self.getPrice()
        oferta = float(p.get('bids')[0].get('price'))
        mercado = 'neutro'
        inicio = oferta
        v_min = oferta
        v_max = oferta
        for i in range(t):
            time.sleep(1)
            p = self.getPrice()
            fin = float(p.get('bids')[0].get('price'))
            if fin < v_min:
                v_min = fin
            if fin > v_max:
                v_max = fin
        if fin > inicio:
            mercado = 'bull'
        if fin < inicio:
            mercado = 'bear'
        else:
            mercado == 'neutro'
        intencion = (v_max - fin) - (inicio - v_min)
        return v_min, inicio, v_max, fin, mercado, intencion

    def predictPrices(self, q):
        x = []
        try:
            with open('training.json', 'r') as f:
                x = json.load(f)
        except:
            pass
        i = 0
        j = 1
        k = 1
        tmp = 0
        kc = 0
        kc_max = 0
        kc_max_numpy = numpy.zeros(20, dtype=int)
        mercado = 'neutro'
        intencion = 0
        suma = 0
        while 1:
            p = self.getPrice()
            oferta = float(p.get('bids')[0].get('price'))
            demanda = float(p.get('asks')[0].get('price'))
            precio = [oferta, demanda]
            if len(x) > N:
                v_min, inicio, v_max, fin, mercado, intencion = self.getVela(TICK)
                i += 1
                x = x[-N:]
                t = torch.tensor([x])
                y_pred = model(t)
                #print(y_pred.tolist())
                bid_pred = y_pred.tolist()[0][0][0]
                ask_pred = y_pred.tolist()[0][1][0]
                tendencia = bid_pred-tmp
                prediccion = False
                print('[ Nro predicciones: ' + str(suma)+' ]')
                print('[ fecha: ' + str(p.get('time'))+' ]')
                print('[ prediccion oferta: ' + str(bid_pred)+' ]')
                print('[ prediccion demanda: ' + str(ask_pred)+' ]')
                print('[ ' + str(mercado)+' ][ min: ' + str(v_min)+' ][ inicio: ' + str(inicio)+' ][ fin: ' + str(fin)+' ][ max: ' + str(v_max)+' ]')
                if tendencia > 0 and mercado == 'bull' and intencion >= 0:
                    prediccion = True
                    print('[ Sube ][ '+str(tendencia)+' ]')
                    print('\a')
                elif tendencia < 0 and mercado == 'bear' and intencion <= 0:
                    prediccion = True
                    print('[ Baja ][ '+str(tendencia)+' ]')
                    print('\a')
                print('[ '+str(i)+' ]------------------------------------------------------ ')
                #time.sleep((TICK/4)*3)
                q.get()
                p = self.getPrice()
                oferta = float(p.get('bids')[0].get('price'))
                demanda = float(p.get('asks')[0].get('price'))
                precio = [oferta, demanda]
                y = torch.tensor(precio)
                if prediccion:
                    if(((bid_pred-tmp) > 0 and (oferta-tmp) > 0) or ((bid_pred-tmp) < 0 and (oferta-tmp) < 0)) and oferta != tmp:
                        j += 1
                        if kc > kc_max:
                            kc_max_numpy[kc-1] += 1
                        kc = 0
                    else:
                        k += 1
                        kc += 1
                suma = (j+k)
                percent = (100*j)/suma
                print('[ real oferta: ' + str(oferta)+' ]')
                print('[ real demanda: ' + str(demanda)+' ]')
                print('[ mercado: ' + str(mercado)+' ][ intencion: ' + str(intencion)+' ]')
                print('[ aciertos: '+str(j)+' ][ desaciertos: '+str(k)+' ]')
                print('[ desaciertos consecutivos: '+str(kc)+' ]')
                print('[ maximos desaciertos consecutivos: '+str(kc_max_numpy)+' ]')
                print('[ acertividad: '+str(int(percent))+'% ]')
                print('[ dif prediccion: '+str(bid_pred-tmp)+'][ dif real: '+str(oferta-tmp)+' ]')
                loss = loss_fn(y_pred, y)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                tmp = oferta
                torch.save(model.state_dict(), PATH)
                with open('training.json', 'w') as training_file:
                    json.dump(x, training_file)
            x.append(precio)






if __name__ == '__main__':
    trader = Trader()
    trader.main()
