import time

from PyQt5.QtCore import QThread, pyqtSignal


class TradingThread(QThread):
    signal = pyqtSignal()

    def __init__(self, q):
        QThread.__init__(self)
        self.q = q
        self.mensaje = ''

    def run(self):
        while 1:
            self.mensaje = self.q.get()
            self.signal.emit()